package handler

import (
	"GoNet/core"
	"fmt"
)

type HandlerFilter struct {
	*core.Filter
}

//session创建
func (f *HandlerFilter) SessionCreate(session *core.Session, serverEngin *core.ServerEngin) {
	fmt.Println("有一个新连接进入")

	serverEngin.SessionManager.PrintSessionMap()
}

//收到消息
func (f *HandlerFilter) MessageRecieved(session *core.Session, obj interface{}, serverEngin *core.ServerEngin) {
	message := obj.(*core.Message)

	handler := serverEngin.HandlerBodyManager.GetHandler(&message.Type)
	handler(message)
}

//客户端断开连接
func (f *HandlerFilter) SessionClosed(session *core.Session, serverEngin *core.ServerEngin) {
	fmt.Println("有客户端断开连接")
	serverEngin.SessionManager.PrintSessionMap()
}

//读空闲
func (f *HandlerFilter) ReadTimeIdle(session *core.Session) {
	//fmt.Println("读空闲")

}

//写空闲
func (f *HandlerFilter) WriteTimeIdle(session *core.Session) {
	//fmt.Println("写空闲")
}
