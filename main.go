// GoNet project main.go
package main

import (
	"GoNet/core"
	"GoNet/logic/code"
	"GoNet/logic/handler"
	"runtime"
)

//import (

//	"fmt"
//)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU() + 1)
	handlerBodyManager := core.NewHandlerBodyManager()
	handlerBodyManager.Init()
	engin := &core.ServerEngin{
		HandlerBodyManager: handlerBodyManager,
		SessionManager:     core.NewSessionManager(),
		ServerShutdown:     make(chan bool, 1),
		FilterChan:         new(core.FilterChan),
		BytePoll:           core.NewMPool(4096),
	}

	//解码
	decode := &code.ClientPbDecodeFilter{new(core.Filter)}

	engin.FilterChan.AddFilter(decode)
	//handler
	hf := &handler.HandlerFilter{new(core.Filter)}
	engin.FilterChan.AddFilter(hf)
	//编码
	encode := &code.ClientPbEncodeFilter{new(core.Filter)}
	engin.FilterChan.AddFilter(encode)

	engin.Start()
}
