package core

import (
	"fmt"
	"sync"
)

type SessionManager struct {
	SessionMap map[int64]*Session
}

func NewSessionManager() *SessionManager {
	return &SessionManager{make(map[int64]*Session)}
}

var sessionMapLock sync.RWMutex

//添加session
func (sessionMap *SessionManager) Add(session *Session) {
	defer sessionMapLock.Unlock()
	sessionMapLock.Lock()
	sessionMap.SessionMap[session.SessionId] = session
}

//删除session
func (sessionMap *SessionManager) Remove(sessionId int64) *Session {
	defer sessionMapLock.Unlock()
	sessionMapLock.Lock()
	var session *Session = sessionMap.SessionMap[sessionId]
	delete(sessionMap.SessionMap, sessionId)
	return session
}

/**获得session**/
func (sessionMap *SessionManager) GetSession(sessionId int64) *Session {
	defer sessionMapLock.RUnlock()
	sessionMapLock.RLock()
	return sessionMap.SessionMap[sessionId]
}

//打印当前session数量
func (sessionMap *SessionManager) PrintSessionMap() {
	fmt.Println("当前用户数量", len(sessionMap.SessionMap))
}
