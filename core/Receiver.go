package core

func CoreRecover() {
	if err := recover(); err != nil {
		CoreLogger.LogStack("core error:%v", err)
	}
}
