package core

//业务接口
type IHandler interface {

	//session创建
	SessionCreate(session *Session, serverEngin *ServerEngin)

	//收到消息
	MessageRecieved(session *Session, message *Message, serverEngin *ServerEngin)

	//客户端断开连接
	SessionClosed(session *Session, serverEngin *ServerEngin)

	//读空闲
	ReadTimeIdle(session *Session)

	//写空闲
	WriteTimeIdle(session *Session)
}
