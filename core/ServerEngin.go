package core

import (
	"GoNet/log"
	"fmt"
	"net"
	"os"
)

var CoreLogger = log.NewLogger(os.Stdout)

//服务器核心类
type ServerEngin struct {
	HandlerBodyManager *HandlerBodyManager
	SessionManager     *SessionManager
	ServerShutdown     chan bool
	FilterChan         *FilterChan
	BytePoll           *MPool
}

func (serverEngin *ServerEngin) Start() {
	listener, err := net.ListenTCP("tcp", &net.TCPAddr{net.ParseIP("0.0.0.0"), 32000, ""})
	if err != nil {
		fmt.Println("error listening:", err.Error())
		os.Exit(1)
	}
	defer listener.Close()
	fmt.Println("开始监听")
	for {
		c, error := listener.Accept()
		if error != nil {
			break
		}
		session := NewBufferSession(c, serverEngin)
		//将session加入session管理器
		serverEngin.SessionManager.Add(session)
		serverEngin.FilterChan.Head.SessionCreate(session, serverEngin)
		go session.newListener(serverEngin)
	}
}

func (serverEngin *ServerEngin) InitSessionIdle() {

	//	go func(serverEngin *ServerEngin) {
	//		for {
	//			select {
	//			case <-serverEngin.ServerShutdown:
	//				break
	//			default:
	//				time.Sleep(1 * time.Second)
	//				sessionMap := serverEngin.SessionManager.SessionMap
	//				now := time.Now().Unix()
	//				for _, v := range sessionMap {
	//					if now > (v.LastReadTime + v.ReadTimeIdle) {
	//						v.ReadTimeOutChan <- true
	//						v.LastReadTime = now
	//					}
	//					if now > (v.LastWriteTime + v.WriteTimeIdle) {
	//						v.WriteTimeOutChan <- true
	//						v.LastWriteTime = now
	//					}
	//				}

	//			}

	//		}

	//	}(serverEngin)

}
