package core

import "code.google.com/p/goprotobuf/proto"

type Message struct {
	Type        int32
	ByteBody    []byte
	Body        proto.Message
	Session     *Session
	AccountList *[]int64
}
